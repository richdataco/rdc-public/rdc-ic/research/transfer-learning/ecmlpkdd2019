# Copyright 2019 Rich Data Corporation Pte Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

'''
Build Transfer Learning experiment Transfer Models and run experiments on them
'''

import os
import traceback
import sys
import getopt

import pandas as pd
import numpy as np

from keras.models import Model, load_model
from keras.layers import Dense, Input, concatenate
from sklearn.model_selection import StratifiedKFold

from allcommon import weight_balance, xy_split_cols, \
    resample_without_replacement_np, calc_gini, YearRange, SMALL_BUS, \
    readLendingClubDataFrame


def read_data_lc2(inputFilename, outputFilename, trim_stdev, optDf = None):
    '''
    Read lending data
    return Dataframe
    '''
    print("READ: no adjustment, trim stdev=" + str(trim_stdev))
    if optDf is None:
        df = pd.read_csv(inputFilename)
    else:
        df = optDf.copy()
#     cols = df.columns.tolist()
    df = df.apply(pd.to_numeric)
    print('1. before=' + str(df.shape))
    df = df[np.abs(df.revol_util_n - df.revol_util_n.mean()) <= (trim_stdev * df.revol_util_n.std())]
    print('2. revol=' + str(df.shape))
    df = df[np.abs(df.int_rate_n - df.int_rate_n.mean()) <= (trim_stdev * df.int_rate_n.std())]
    print('3. int rate=' + str(df.shape))
    df = df[np.abs(df.installment_n - df.installment_n.mean()) <= (trim_stdev * df.installment_n.std())]
    print('4. instalment=' + str(df.shape))
    df = df[np.abs(df.tot_hi_cred_lim_n - df.tot_hi_cred_lim_n.mean()) <= (trim_stdev * df.tot_hi_cred_lim_n.std())]
    print('5. tot hi cred lim=' + str(df.shape))
    df = df[np.abs(df.emp_length_n - df.emp_length_n.mean()) <= (trim_stdev * df.emp_length_n.std())]
    print('6. emp length=' + str(df.shape))
    df = df[np.abs(df.dti_n - df.dti_n.mean()) <= (trim_stdev * df.dti_n.std())]
    print('7. dti=' + str(df.shape))
    df = df[np.abs(df.avg_cur_bal_n - df.avg_cur_bal_n.mean()) <= (trim_stdev * df.avg_cur_bal_n.std())]
    print('8. avg cur bal=' + str(df.shape))
    df = df[np.abs(df.all_util_n - df.all_util_n.mean()) <= (trim_stdev * df.all_util_n.std())]
    print('9. all util=' + str(df.shape))
    df = df[np.abs(df.acc_open_past_24mths_n - df.acc_open_past_24mths_n.mean()) <= (trim_stdev * df.acc_open_past_24mths_n.std())]
    print('10. acc open past=' + str(df.shape))
    df = df[np.abs(df.annual_inc_n - df.annual_inc_n.mean()) <= (trim_stdev * df.annual_inc_n.std())]
    print('11. annual inc =' + str(df.shape))
    df = df[np.abs(df.loan_amnt_n - df.loan_amnt_n.mean()) <= (trim_stdev * df.loan_amnt_n.std())]
    print('12. loan amnt=' + str(df.shape))
    df = df[np.abs(df.cover - df.cover.mean()) <= (trim_stdev * df.cover.std())]
    # save to csv
    print('13. after=' + str(df.shape))
    df.to_csv(outputFilename + '_trimmed_not_normalised', sep = ',')
    result = df.copy()
    for feature_name in df.columns:
        max_value = df[feature_name].max()
        min_value = df[feature_name].min()
        norm_range = max_value - min_value
        if norm_range == 0:
            norm_range = 1
        result[feature_name] = (df[feature_name] - min_value) / norm_range
    df = result
    df.to_csv(outputFilename + '_dataset_trimmed_normalised', sep = ',')
    dataset = df.values
    return dataset


def setup_transfer_learning(common_input, modelbase, trainable):
    '''
    Setup the Transfer Learning model
    return the last layer output
    '''
    # all not trainable
    one_layer = modelbase.layers[1](common_input)  # connecting the 1st hidden layer to a new common input layer

    remove_the_output_layer = 1  # remove_the_output_layer = 0 #mean not removing the output layer
    for i in range(2, len(modelbase.layers) - remove_the_output_layer):  # -1, the output layer is excluded, without -1, the output layer is included
        one_layer = modelbase.layers[i](one_layer)
    output = one_layer

    # renaming all hidden layers, n fold cross validation in modelx (the base model), does not reset the sequence
    j = 0
    for layer in modelbase.layers:
        if j >= 1:  # we skip input layers, just renaming hidden layers
            layer.name = "xdense_" + str(j) + "_" + str(trainable)  # please note, this name needs to be aligned with the test_name below
        j = j + 1

    k = 0
    # trainable = 11  # all layer 1..10 are false
    for layer in modelbase.layers:
        # test_name = "dense_" + str(k)
        # if (k >= trainable) and (test_name == str(layer.name)):
        is_trainable = k >= trainable
        if (k >= trainable):
            layer.trainable = True
        else:
            layer.trainable = False
        # layer.name = layer.name + "_" + str(trainable)
        print(str(layer.name) + "::layer[" + str(k) + "] trainable " + str(is_trainable))
        k = k + 1
    return output


def parallel_model(
                    cv_fold,
                    inputDir,
                    outputDir,
                    subfolder,
                    model_name,
                    basemodelInputDir,
                    basemodel_filename,
                    n_epochs,
                    n_row,
                    n_fold,
                    n_batch,
                    stdev_trim,
                    my_verbose,
                    description,
                    filename,
                    number_of_nodes,
                    number_of_cols,
                    number_of_all_layers,
                    list_of_seeds,
                    is_pruned,
                    prune_threshold,
                    data_source,
                    modifier_start_block,
                    modifier_end_block,
                    n_inputs,
                    n_outputs,
                    optDf = None
                    ):
    '''
    Build Parallel models
    return average model and associated performance
    '''
    number_of_models = len(list_of_seeds) * n_fold
    performance = pd.DataFrame(index = np.arange(0, number_of_models), columns = ('repeat', 'description', 'iteration', 'training', 'test', 'gini'))

    number_of_outputs = n_outputs
    number_of_inputs = n_inputs
    repeat = 0
    # starting_block = number_of_all_layers+0 #from 1 to number_of_all_layers + 1
    # ending_block = number_of_all_layers+2

    starting_block = number_of_all_layers + modifier_start_block  # from 1 to number_of_all_layers + 1
    ending_block = number_of_all_layers + modifier_end_block  # for example hidden layer 3, layer 0 is input, 1,2,3 hidden layers, 4 output,
    print("======================================> starting block=" + str(starting_block) + "  ending block=" + str(ending_block))
    # start block = 3 + 2, ending block = 3 + 3, so only run for i=5, not continue to i=6

    # starting block, ending block, +1, +2, output only trainable, Gini 0.34980, Std 0.00948
    # +2, +3, output only trainable
    # +0, +1  layer 3 is trainable, layer 3 is the last hidden layer, layer 4 is the output layer
    number_of_blocks = ending_block - starting_block

    # for all repeat
    for a_random_seed in list_of_seeds:
        np.random.seed(a_random_seed)
        print(model_name + " ==> random seed=" + str(a_random_seed))
        transfer_epochs = n_epochs
        transfer_batch = n_batch
        iteration = 0
        if data_source == 'fullsb':
            full_data = read_data_lc2(inputDir + filename, outputDir + filename, stdev_trim, optDf)

        print("sb fulldata=" + str(full_data.shape))
        subset_data = resample_without_replacement_np(full_data, n_row)
        print("sb fulldata=" + str(subset_data.shape) + "  n_row=" + str(n_row))
        Xfull, Yfull = xy_split_cols(subset_data, number_of_cols)
        skf = StratifiedKFold(n_splits = n_fold, random_state = None, shuffle = False)
        print("number of splits = " + str(skf.get_n_splits(Xfull, Yfull)))
        class_weights = weight_balance(Yfull)

        fold_count = 0
        # for number of fold in cross validation
        basemodel_type = "static"  # base model can be dynamic, created on the fly

        for train_index, test_index in skf.split(Xfull, Yfull):
            Xtrain, Xtest = Xfull[train_index], Xfull[test_index]
            Ytrain, Ytest = Yfull[train_index], Yfull[test_index]
            XYtrain = np.concatenate((Xtrain, Ytrain[:, None]), axis = 1)
            print("Xtrain.shape=" + str(Xtrain.shape) + "  Ytrain.shape=" + str(Ytrain.shape) + "  XYtrain.shape=" + str(XYtrain.shape))

            if (basemodel_type == "static"):
                path_filename = basemodelInputDir + basemodel_filename
            else:
                print ("basemodel is either static or dynamic")
                exit(0)
            fold_count = fold_count + 1

            print("Loading base model:", path_filename)
            modelbase = [None] * (number_of_all_layers + 3)

            for i in range(starting_block, ending_block):
                print("loading model [" + str(i) + "] " + path_filename)
                modelbase[i] = load_model(path_filename)
            common_input = Input(shape = (number_of_inputs,))  # the input layer
            # combine all outputs
            output = [None] * (number_of_all_layers + 3)
            outputs = list()

            for i in range(starting_block, ending_block):  # if all layers are 1,2,3..10, then this loop should from 1,2,3,..10,11, so need to be +2 as the last range is exclusive, range(1,12) will iterate from 1..11 as 12 is exclusive
                output[i] = setup_transfer_learning(common_input, modelbase[i], i)  # layer>=i then trainable, initial i=1, all trainable, then 2,3,...trainable,
                print("layer>=" + str(i) + " then trainable")
                # then ...,then finally 9,10 trainable, 10 trainable, none trainable
                outputs.append(output[i])
                last_output = output[i]

            # if only one output, no need to concatenate
            if number_of_blocks > 1:
                merge_one = concatenate(outputs)
            elif number_of_blocks == 1:
                merge_one = last_output
            else:
                print("number of hidden layers have to >= 1")
                exit(0)

            merge_one.trainable = False
            output = Dense(number_of_outputs, activation = 'sigmoid')(merge_one)
            output.trainable = False
            print("summary model transferred after trainable flag update")
            modeltransferred = Model(inputs = common_input, outputs = output)
            modeltransferred.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
            print("start transferred model")
            print(modeltransferred.summary())
            print("end transferred model")

            modeltransferred.fit(Xtrain, Ytrain, epochs = transfer_epochs, batch_size = transfer_batch, class_weight = class_weights, verbose = my_verbose)
            Ypred = (modeltransferred.predict(Xtest)).flatten()
            print("transferred model::refreshed repeat=" + str(repeat) + "  iteration=" + str(iteration))
            modeltransferred.summary()
            model_index = (repeat * n_fold) + iteration
            os.makedirs(outputDir + subfolder, exist_ok = True)
            modeltransferred.save(outputDir + subfolder + model_name + "_" + cv_fold + "_" + str(model_index) + ".h5")
            performance.loc[model_index] = repeat, description, iteration, Ytrain.size, Xtrain.size, calc_gini(Ypred, Ytest)
            print("TRAIN:", train_index.size, "  TEST:", test_index.size, "  GINI:", calc_gini(Ypred, Ytest))
            pred_vs_actual = pd.DataFrame(np.column_stack([Ypred, Ytest]), columns = ['Yprediction', 'Yactual'])
            pred_vs_actual.to_csv(outputDir + subfolder + "pred_vs_actual_" + model_name + "_" + cv_fold + "_" + str(model_index) + ".csv", sep = ',', index = False)
            iteration = iteration + 1
        repeat = repeat + 1
    avg_gini = performance.loc[:, "gini"].mean()
    error = [None] * (number_of_models)
    min_error = 999
    for i in range(0, number_of_models):
        error[i] = abs(performance.loc[i, "gini"] - avg_gini)
        if error[i] < min_error:
            min_error = error[i]
    model_filename = outputDir + subfolder + model_name + "_" + cv_fold + "_" + str(model_index) + ".h5"
    print("About to load average model from " + model_filename)
    average_model = load_model(model_filename)
    return average_model, performance


def usage():
    print ('Usage: ' + sys.argv[0] + ' startYear endYear models numFolds seedList\n' +
           'Build transferred model for data from startYear to endYear\n' +
           'models = w|wx|wxy|wxyz\n' +
           'seedList = Ex: 7,8,9')
    sys.exit(2)


def main():

    try:
        optlist, args = getopt.getopt(sys.argv[1:], 'hc')

        useCar = False
        for o, a in optlist:
            if o == "-h":
                usage()
            elif o == "-c":
                useCar = True
            else:
                assert False, "unhandled option " + o

        if len(args) != 5:
            usage()

        yearRange = YearRange(int(args[0]), int(args[1]))
        models = args[2]
        n_fold = int(args[3])
        list_of_seeds = [int(s) for s in (args[4]).split(',')]

        scriptDir, scriptFilename = os.path.split(os.path.abspath(__file__))
        inputDir = scriptDir + '/input/'

        description = "training on multiple loan, transfer, test on multiple loan"
        subfolder = "minus1_plus1/"
        outputDir = scriptDir + '/output/'
        filename = "lending_club_2012-2018_small_business.csv"

        outputDir = scriptDir + '/output/'
        dataSource = 'credit_debt'

        dataSource = 'credit_debt'
        basemodelInputDir = outputDir + 'model/' + dataSource + '/'

#         list_of_seeds = [7]
        repeat = len(list_of_seeds)
        prefix = "transferred"
        cv_fold = prefix + "_" + str(repeat) + "repeat_cv"

        stdev_trim = 10000  # The std deviation for trimming outliers

        data_source = "fullsb"
        is_pruned = False
        prune_threshold = 0.00
        number_of_cols = 17  # number of columns, the last column is the outcome
        model_name = "model_test_deep_learning"
        n_row = 13794
        number_of_nodes = 32
        number_of_all_layers = 3  # hidden layer only, +1 output layer, +1 input layer
        my_verbose = 0
        n_inputs = 16
        n_ouputs = 1

        n_epochs = 100
        n_nodes = 32
        n_layers = 3

        list_of_folds = [n_fold]
        len_fold = len(list_of_folds)

        # Model U
        model_name = "base_L" + str(n_layers) + "_N" + str(n_nodes) + "_E" + str(n_epochs)
        basemodel_filename = "model_average_" + model_name + '_' + str(n_fold) + 'fold_' + str(repeat) + 'repeat_cv.h5'

        file_h5 = "model_average_" + model_name + "_" + cv_fold + ".h5"
        file_gini = "model_performance_" + model_name + "_" + cv_fold + ".csv"

        description = prefix

        list_of_epochs = [200]
        len_epochs = len(list_of_epochs)

        list_of_batches = [100]
        len_batch = len(list_of_batches)

        # for this setup, the values are w:1,2;  wx: 0,2;  wxy:-1,2;  wxyz:-2,2
        if models == 'w':
            modifier_start_block = 1
            modifier_end_block = 2
        elif models == 'wx':
            modifier_start_block = 0
            modifier_end_block = 2
        elif models == 'wxy':
            modifier_start_block = -1
            modifier_end_block = 2
        elif models == 'wxyz':
            modifier_start_block = -2
            modifier_end_block = 2
        else:
            raise Exception('Unknown models=' + str(models))

        list_of_optimizers = ['adam']
        len_optimizer = len(list_of_optimizers)

        combo = len_epochs * len_batch * len_fold * len_optimizer

        optimisation = pd.DataFrame(index = np.arange(0, combo), columns = (
        'trial', 'trial_desc', 'epoch', 'batch', 'fold', 'optimizer', 'avg_gini', 'std_dev'))

        optDf = readLendingClubDataFrame(inputDir, yearRange, SMALL_BUS, useCar)

        trial_index = 0
        for one_epoch in list_of_epochs:
            for one_batch in list_of_batches:
                for one_fold in list_of_folds:
                    for one_optimizer in list_of_optimizers:
                        n_epochs = one_epoch
                        n_batch = one_batch
                        n_fold = one_fold

                        repeat = len(list_of_seeds)
                        cv_fold = str(n_fold) + "fold_" + str(repeat) + "repeat_" + str(one_epoch) + "epochs_cv"
                        file_gini = "model_performance_" + model_name + "_" + cv_fold + ".csv"
                        trial_desc = "epochs" + str(one_epoch) + "_batch" + str(one_batch) + "_fold" + str(one_fold) + "_optimizer" + (one_optimizer)

                        model, performance = parallel_model(
                                              cv_fold,
                                              inputDir,
                                              outputDir,
                                              subfolder,
                                              model_name,
                                              basemodelInputDir,
                                              basemodel_filename,
                                              n_epochs,
                                              n_row,
                                              n_fold,
                                              n_batch,
                                              stdev_trim,
                                              my_verbose,
                                              description,
                                              filename,
                                              number_of_nodes,
                                              number_of_cols,
                                              number_of_all_layers,
                                              list_of_seeds,
                                              is_pruned,
                                              prune_threshold,
                                              data_source,
                                              modifier_start_block,
                                              modifier_end_block,
                                              n_inputs,
                                              n_ouputs,
                                              optDf = optDf)

                        model.save(outputDir + subfolder + file_h5)  # note only the last model is saved
                        performance.to_csv(outputDir + subfolder + file_gini, sep = ',', index = False)
                        # Print the average Gini
                        text = "Desc:" + description + "  Number of rows:" + str(n_row) + "  Average Gini of " + str(n_fold) + " fold x " + str(repeat) + " repeat cross validation experiments is " + str(performance.loc[:, "gini"].mean()) + " with Standard Deviation:" + str(performance.loc[:, "gini"].std())
                        print(text)

                        optimisation.loc[trial_index] = trial_index, trial_desc, one_epoch, one_batch, one_fold, one_optimizer, performance.loc[:, "gini"].mean(), performance.loc[:, "gini"].std()
                        trial_index = trial_index + 1
        summary_file = outputDir + subfolder + prefix + "_experiment_" + str(n_row) + ".csv"
        print(summary_file)
        optimisation.to_csv(outputDir + subfolder + prefix + "_experiment_" + str(n_row) + ".csv")

    except getopt.GetoptError as err:
        # print help information and exit:
        print (str(err))
        print (traceback.format_exc())
        sys.exit('Failed: %s' % (err))

    except Exception as e:
        # print help information and exit:
        print (traceback.format_exc())
        sys.exit('Failed: %s' % (e))


# Run main() if explicitly loaded (as opposed to being imported)
if __name__ == "__main__":
    main()

